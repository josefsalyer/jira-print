require 'oga'
class Card
  attr_accessor :id, :title, :raw, :type

  @id = 'NONE'
  @title = 'NONE'
  @raw = ''
  @type = ''

  def initialize(item=nil)
    if item
      a,b,c,d = item.title.match(/(\[)([A-Z0-9]{4}-[0-9]{0,5})(\] )(.+)/m).captures
      @id = b
      @title = d
      a,c = nil

      @raw = Oga.parse_html(item.description)
      type = @raw.at_xpath('/table[2]/tr[1]/td[2]/text()')

      @type = type ? type.text.strip : ""
    end


  end
end

=begin
    doc = Oga.parse_html(card.description)

    labels = []
    doc.css('td').each do |td|

      td.children.each do |child|
        # first grab the labels from the children
        obj = {}
        if child.class == Oga::XML::Element
          if child.children.first.class == Oga::XML::Text && child.children.first.text.include?(':')
            label = child.children.first.text.gsub(":","")
            #pp child.siblings
            obj[label] = ""#child.children.first.text

          end
          # add the key value pair to the labels array
          labels.push(obj)
        end
      end
    end
=end

    #pp labels
