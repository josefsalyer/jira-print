# polls the jira instance for cards and returns new ones
require 'jira_configuration'
require 'rss'
require 'open-uri'
require 'card'

class CardPoller
  attr_accessor :config
  attr_accessor :cards

  def initialize
    @config = JiraConfiguration.new
  end

  def poll
    logDateTime
    @cards = Array.new
    open(@config.uri) do |rss|
      feed = RSS::Parser.parse(rss)

      feed.items.each do |item|
        card = Card.new(item)
        @cards.push(card)
      end
    end
  end

  def logDateTime
    log = File.new('lastrun.log', 'w+')

    now = DateTime.now.strftime('%F %H:%M')

    log.write(now.chomp())

  end

end
