# manages the configuration information for connecting to a jira instance
require 'yaml'
require 'pp'

class JiraConfiguration
  attr_accessor :url
  attr_accessor :project_id
  attr_accessor :path
  attr_accessor :query

  def initialize
    file        = File.new('config/Jira')
    config      = YAML.parse(file.read).to_ruby
    @url        = config['url']
    @project_id = config['project_id']
    @path       = "/sr/jira.issueviews:searchrequest-rss/temp/SearchRequest.xml"
    @query      = "jqlQuery=project%3D#{@project_id}+and+created%3E%3D%222016-09-21+12%3A00%22&tempMax=1000"
  end

  def uri
    uri = "#{url}#{path}?#{query}"
    uri
  end
end
