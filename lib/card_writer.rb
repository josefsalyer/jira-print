require 'prawn'
# writes a single or multiple cards out to a pdf

class CardWriter
  def write(card)

    height = 288
    width = 432
    margin = 36
    type_inset = 110
    # 4x6 @72dpi
    size = [height,width]

    Prawn::Document.generate("#{card.id}.pdf",
        :page_size=>size,
        :page_layout=>:landscape
      ) do # card id
        font("Helvetica", :size => 26)
        text "#{card.id}", :align => :left

        # card type
        font("Helvetica", :size => 26)
        text_box "#{card.type}", :at => [width - margin * 2 - type_inset, y - 5]

        # card title
        font("Helvetica", :size => 22)
        text_box "#{card.title}", :at =>[0,188],
          :width => (width - (2 * margin)),
          :height => (height - (2 * margin)),
          :overflow => :shrink_to_fit
    end
  end
end
