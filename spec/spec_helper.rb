require 'webmock/rspec'
WebMock.disable_net_connect!(allow_localhost: true)

$LOAD_PATH.unshift File.dirname(__FILE__) + '/../src'



RSpec.configure do |config|
  # stub http requests to jira.cas.org https://robots.thoughtbot.com/how-to-stub-external-services-in-tests
  config.before(:each) do
    xml = File.open('./spec/jira-response.xml', 'r')
    response = xml.read
    stub_request(:get, /jira.cas.org/)
      .with(headers: {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Ruby'})
      .to_return(:status => 200, :body => response, :headers => {})
  end

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
end
