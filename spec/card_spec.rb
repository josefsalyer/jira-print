require 'spec_helper'
require 'card'
require 'rss'
require 'pp'
require 'oga'

describe 'Card' do

  before(:each) do
    file = File.open('./spec/item.xml','r')
    desc = file.read
    xml = '<rss version="2.0"> <channel> <title>CAS JIRA</title> <link>https://jira.cas.org/secure/IssueNavigator.jspa?reset=true&amp;jqlQuery=project%3DC3PO+and+created%3E%3D%222016-09-21+12%3A00%22</link> <description>An XML representation of a search request</description> <language>en-us</language> <issue start="0" end="21" total="21" /><build-info> <version>7.0.5</version> <build-number>70114</build-number> <build-date>17-12-2015</build-date> </build-info>' + desc + ' </channel>'
    @rss = RSS::Parser.parse(xml)
  end

  context '.new' do
    it 'should be instantiable' do
      expect{ Card.new }.not_to raise_error
      card = Card.new
      expect(card).not_to be_nil
    end

    it 'should accept an xml string for instantiation' do
      expect{ Card.new(@rss.items[0]) }.not_to raise_error
    end

    it 'should set the card id and title' do
      expect(@rss.items.count).to be(1)
      card = Card.new(@rss.items[0])
      expect(card.id).to match('C3PO-1091')
      expect(card.title).to match('Buildtools container fails working projects')
    end
  end

  context '.parse' do
    it 'should get the card type' do
      expect(@rss.items.count).to be(1)
      item = @rss.items[0]
      expect(item.description).not_to be_nil
      card = Card.new(item)
      expect(card.raw).not_to be_nil
      expect(card.type).to match('Bug')
    end
  end
end
