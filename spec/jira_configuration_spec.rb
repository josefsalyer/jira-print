require 'spec_helper'
require 'jira_configuration'

describe 'Jira Configuration' do
  context '.new' do
    it 'should be instantiable' do
      config = JiraConfiguration.new

      expect(config).not_to be_nil
    end

    it 'should set the url' do
      expected = 'https://jira.cas.org'
      config = JiraConfiguration.new
      expect(expected).to match(config.url)
    end

    it 'should set the project id' do
      expected = 'C3PO'
      config = JiraConfiguration.new
      expect(expected).to match(config.project_id)
    end
  end
end
